var xhr = {
    sendXHR: function(xhrObject, requestMethod, URL, callBack){
        var xhrReq = new XMLHttpRequest();
        xhrReq.open(requestMethod, encodeURI(URL));
        if(requestMethod=="POST")
            xhrReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        if(requestMethod=="GET")
            xhrReq.setRequestHeader('Content-Type', 'application/json');
        xhrReq.onload = function() {
            if (xhrReq.status === 200)
                callBack(JSON.parse(xhrReq.responseText));
            else
                console.log('Request failed.  Returned status of ' + xhrReq.status);
        };
        console.log("SENDING: "+JSON.stringify(xhrObject));
        xhrReq.send(JSON.stringify(xhrObject));
    }
};