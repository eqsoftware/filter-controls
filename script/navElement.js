var filterBuilder={
    clear:function(appendToo){
        container=document.querySelector(appendToo);
        container.innerHTML='';
    },
    handleOpenClose:function(elementName, icon){
        var elm=document.querySelector("#"+elementName+"Items");
        if(elm.classList.contains("open")){
            elm.classList.remove("open");
            icon.classList.remove("close");
            icon.classList.add("open");
        } else {
            elm.classList.add("open");
            icon.classList.remove("open");
            icon.classList.add("close");
        }
        event.preventDefault();
        return false;
    },
    createRangeSlider:function(appendToo, elementName, heading, headingExtension, objectData, singleOption, opened) {
        container=document.querySelector(appendToo);
        var rangeContainer=document.createElement("div");
        rangeContainer.className="rangeFilter";
        var openerIcon=document.createElement("div");
        openerIcon.className="filterOpenerIcon"+(opened?" close":" open");
        openerIcon.addEventListener("mouseup", function(){filterBuilder.handleOpenClose(elementName, this);}, false);
        openerIcon.addEventListener("touchend", function(){filterBuilder.handleOpenClose(elementName, this);}, false);
        rangeContainer.appendChild(openerIcon);
        var containerHeading=document.createElement("h3");
        containerHeading.innerHTML=heading + (headingExtension && headingExtension.length>0?" ("+headingExtension+")":"");
        var rangeSliderItems=document.createElement("div");
        rangeSliderItems.className="rangeSliderItems"+(opened?" open":"");
        rangeSliderItems.id=elementName+"Items";
        var rangeSlider=document.createElement("div");
        rangeSlider.className="rangeSlider noUi-extended";
        rangeSlider.id=elementName+"Slider";
        var rangeFilterBoxes=document.createElement("div");
        rangeFilterBoxes.className="rangeFilterBoxes";
        if(singleOption==false){
            var lowInput=document.createElement("input");
            lowInput.id=elementName+"Low";
            lowInput.name=elementName+":min";
            lowInput.className="low";
            var textSpan=document.createElement("span");
            textSpan.innerHTML="To";
        }
        var highInput=document.createElement("input");
        highInput.id=elementName+"High";
        highInput.name=elementName+":max";
        highInput.className="high";
        rangeContainer.appendChild(containerHeading);
        rangeContainer.appendChild(rangeSliderItems);
        rangeSliderItems.appendChild(rangeSlider);
        rangeSliderItems.appendChild(rangeFilterBoxes);
        if(singleOption==false) {
            rangeFilterBoxes.appendChild(lowInput);
            rangeFilterBoxes.appendChild(textSpan);
        }
        rangeFilterBoxes.appendChild(highInput);
        if(singleOption==false) {
            noUiSlider.create(rangeSlider, {
                start: [objectData.startMin, objectData.startMax],
                connect: true,
                range: {
                    'min': objectData.min,
                    'max': objectData.max
                },
                step: objectData.steps
            });
        } else {
            noUiSlider.create(rangeSlider, {
                start: objectData.startMax,
                connect: "lower",
                range: {
                    'min': objectData.min,
                    'max': objectData.max
                },
                step: objectData.steps
            });
        }
        rangeSlider.noUiSlider.on('set', function(){
            filterBuilder.getFilterData(appendToo);
        });
        rangeSlider.noUiSlider.on('update', function (values, handle) {
            if(singleOption==false)
                if (handle)
                    highInput.value=Math.round(values[handle]);
                else
                    lowInput.value=Math.round(values[handle]);
            else
                highInput.value=Math.round(values[handle]);
        });
        if(singleOption==false) {
            lowInput.addEventListener('change', function () {
                rangeSlider.noUiSlider.set([this.value, null]);
            });
            highInput.addEventListener('change', function () {
                rangeSlider.noUiSlider.set([null, this.value]);
            });
        } else
            highInput.addEventListener('change', function () {
                rangeSlider.noUiSlider.set(this.value);
            });
        container.appendChild(rangeContainer);
        return rangeSlider.noUiSlider;
    },
    createTickList:function(appendToo, elementName, heading, headingExtension, objectData, singleOption, opened) {
        container=document.querySelector(appendToo);
        var tickBoxContainer=document.createElement("div");
        tickBoxContainer.className="tickBoxFilter";
        var openerIcon=document.createElement("div");
        openerIcon.className="filterOpenerIcon"+(opened?" close":" open");
        openerIcon.addEventListener("mouseup", function(){filterBuilder.handleOpenClose(elementName, this);}, false);
        openerIcon.addEventListener("touchend", function(){filterBuilder.handleOpenClose(elementName, this);}, false);
        tickBoxContainer.appendChild(openerIcon);
        var containerHeading=document.createElement("h3");
        containerHeading.innerHTML=heading + (headingExtension && headingExtension.length > 0 ? " ("+headingExtension+")":"");
        tickBoxContainer.appendChild(containerHeading);
        var tickBoxItems=document.createElement("div");
        tickBoxItems.className="tickBoxItems"+(opened?" open":"");
        tickBoxItems.id=elementName+"Items";
        for (var key in objectData) {
            if (objectData.hasOwnProperty(key)) {
                var item=document.createElement("div");
                var itemCheckBox=document.createElement("input");
                itemCheckBox.type="checkbox";
                itemCheckBox.name=itemCheckBox.id=elementName+":"+objectData[key].key;
                itemCheckBox.checked=objectData[key].selected;
                itemCheckBox.disabled=!objectData[key].active;
                if(singleOption){
                    itemCheckBox.addEventListener("change", function(){
                        var inputs=document.querySelector(appendToo).querySelectorAll("input");
                        [].forEach.call(inputs, function(input) {
                            if(input.type=="checkbox" && input.name.match(elementName+":") && input.name!=this.name && input.checked){
                                input.checked=false;
                            }
                        }.bind(this), false);
                        filterBuilder.getFilterData(appendToo);
                    });
                } else
                    itemCheckBox.addEventListener("change", function(){
                        filterBuilder.getFilterData(appendToo);
                    });
                var itemTitle=document.createElement("label");
                itemTitle.setAttribute("for", elementName+":"+objectData[key].key);
                itemTitle.innerHTML=objectData[key].title;
                if (!objectData[key].active)
                    itemTitle.className="disabled";
                item.appendChild(itemCheckBox);
                item.appendChild(itemTitle);
                if (objectData[key].count > 0) {
                    var itemCount=document.createElement("span");
                    itemCount.innerHTML="("+objectData[key].count+")";
                    itemTitle.appendChild(itemCount);
                }
                tickBoxItems.appendChild(item);
            }
        }
        tickBoxContainer.appendChild(tickBoxItems);
        container.appendChild(tickBoxContainer);
        return tickBoxContainer;
    },
    createRatingList:function(appendToo, elementName, heading, headingExtension, objectData, singleOption, opened){
        container=document.querySelector(appendToo);
        var ratingContainer=document.createElement("div");
        ratingContainer.className="ratingFilter";
        var openerIcon=document.createElement("div");
        openerIcon.className="filterOpenerIcon"+(opened?" close":" open");
        openerIcon.addEventListener("mouseup", function(){filterBuilder.handleOpenClose(elementName, this);}, false);
        openerIcon.addEventListener("touchend", function(){filterBuilder.handleOpenClose(elementName, this);}, false);
        ratingContainer.appendChild(openerIcon);
        var containerHeading=document.createElement("h3");
        containerHeading.innerHTML=heading + (headingExtension && headingExtension.length>0?" ("+headingExtension+")":"");
        ratingContainer.appendChild(containerHeading);
        var ratingItems=document.createElement("div");
        ratingItems.className="ratingItems"+(opened?" open":"");
        ratingItems.id=elementName+"Items";
        for (var key in objectData) {
            if (objectData.hasOwnProperty(key)) {
                var item=document.createElement("div");
                var itemCheckBox=document.createElement("input");
                itemCheckBox.type="checkbox";
                itemCheckBox.name=itemCheckBox.id=elementName + ":" + objectData[key].key;
                itemCheckBox.checked=objectData[key].selected;
                itemCheckBox.disabled=!objectData[key].active;
                if(singleOption){
                    itemCheckBox.addEventListener("change", function(){
                        var inputs=document.querySelector(appendToo).querySelectorAll("input");
                        [].forEach.call(inputs, function(input) {
                            if(input.type=="checkbox" && input.name.match(elementName+":") && input.name!=this.name && input.checked)
                                input.checked=false;
                        }.bind(this), false);
                        filterBuilder.getFilterData(appendToo);
                    });
                } else
                    itemCheckBox.addEventListener("change", function(){
                        filterBuilder.getFilterData(appendToo);
                    });
                var itemTitle=document.createElement("label");
                itemTitle.setAttribute("for", elementName+":"+objectData[key].key);
                for(var i=0;i<objectData[key].images.normal;i++){
                    var img=document.createElement("span");
                    img.className="star";
                    itemTitle.appendChild(img);
                }
                for(var i=0;i<objectData[key].images.half;i++){
                    var img=document.createElement("span");
                    img.className="star-half";
                    itemTitle.appendChild(img);
                }
                for(var i=0;i<objectData[key].images.empty;i++){
                    var img=document.createElement("span");
                    img.className="star-empty";
                    itemTitle.appendChild(img);
                }
                if (!objectData[key].active)
                    itemTitle.className="disabled";
                var itemText=document.createElement("div");
                itemText.innerHTML=objectData[key].title;
                itemTitle.appendChild(itemText);
                item.appendChild(itemCheckBox);
                item.appendChild(itemTitle);
                if (objectData[key].count > 0) {
                    var itemCount=document.createElement("span");
                    itemCount.innerHTML="("+objectData[key].count+")";
                    itemText.appendChild(itemCount);
                }
                ratingItems.appendChild(item);
            }
        }
        ratingContainer.appendChild(ratingItems);
        container.appendChild(ratingContainer);
    },
    getFilterData:function(element){
        var json={};
        var inputs=document.querySelector(element).querySelectorAll("input");
        [].forEach.call(inputs, function(input) {
            if(input.type=="checkbox")
                if(input.checked)
                    json["opt:"+input.name]=input.checked;
            else
                json["val:"+input.name]=(filterBuilder.isInt(input.value) ? parseInt(input.value) : input.value);
        });
        xhr.sendXHR(json, "POST", "/testObject/catchXhr.pjson", filterBuilder.showContents);
    },
    buildFilters:function(obj){
        for (var key in obj) {
            if(obj[key].fn=="getFilterData")
                window['filterBuilder'][obj[key].fn](obj[key].appendToo, obj[key].sendToServer);
            else if(obj[key].fn=="clear")
                window['filterBuilder'][obj[key].fn](obj[key].elementName);
            else
                window['filterBuilder'][obj[key].fn](obj[key].appendToo, obj[key].elementName, obj[key].heading, obj[key].headingExtension, obj[key].objectData, obj[key].singleOption, obj[key].opened);
        }
    },
    showContents:function(obj){
        console.log("RECEIVED: "+JSON.stringify(obj));
    },
    isInt:function(value) {
        return !isNaN(value)&&(function(x){return (x|0)===x;})(parseFloat(value));
    }
};