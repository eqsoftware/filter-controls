<!DOCTYPE html>
<html>
<head>
    <title>Filter Controls</title>
    <link href="/css/nouislider.css" rel="stylesheet">
    <link href="/css/core.css" rel="stylesheet">
    <script src="/script/nouislider.min.js"></script>
    <script src="/script/core.js"></script>
    <script src="/script/navElement.js"></script>
    <script src="/script/xhr.js"></script>
</head>
<body>
<div id="navArea" class="navArea">Please Wait...</div>
</body>
</html>
